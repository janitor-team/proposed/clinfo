clinfo (3.0.21.02.21-1) unstable; urgency=medium

  * New upstream release.
  * Let reportbug collect information about installed ICDs.

 -- Andreas Beckmann <anbe@debian.org>  Mon, 22 Feb 2021 17:42:37 +0100

clinfo (3.0.20.11.20-1) unstable; urgency=medium

  * New upstream release.  (Closes: #975339)

 -- Andreas Beckmann <anbe@debian.org>  Sat, 21 Nov 2020 00:58:39 +0100

clinfo (2.2.18.04.06-2) unstable; urgency=medium

  * Avoid misleading notice on loader supporting OpenCL 3.0.
  * Switch to debhelper-compat (= 13).
  * Bump Standards-Version to 4.5.1, no changes needed.

 -- Andreas Beckmann <anbe@debian.org>  Thu, 19 Nov 2020 16:19:53 +0100

clinfo (2.2.18.04.06-1) unstable; urgency=medium

  * New upstream release.
  * Switch to debhelper-compat (= 11).
  * Bump Standards-Version to 4.2.1, no changes needed.

 -- Andreas Beckmann <anbe@debian.org>  Wed, 19 Dec 2018 01:23:44 +0100

clinfo (2.2.18.03.26-1) unstable; urgency=medium

  * New upstream release.
  * Ship the new README.md.

 -- Andreas Beckmann <anbe@debian.org>  Tue, 27 Mar 2018 18:09:33 +0200

clinfo (2.2.18.03.15-1) unstable; urgency=medium

  * New upstream release.
  * Update watch file.
  * Add debian/upstream/metadata.
  * Switch Vcs-* URLs to salsa.debian.org.

 -- Andreas Beckmann <anbe@debian.org>  Sun, 18 Mar 2018 07:29:01 +0100

clinfo (2.2.17.10.25-1) unstable; urgency=medium

  * New upstream release.
    (Closes: #845400, #848169, #848171, #848177, #848182)
  * Upstream license changed from public domain to CC0.
  * Bump Standards-Version to 4.1.3, no changes needed.
  * Switch to debhelper compat level 11.
  * Set Rules-Requires-Root: no.
  * Conflicts/Replaces: fglrx-updates-core.  (LP: #1564753)
  * Add watch file.
  * Enable more hardening.

 -- Andreas Beckmann <anbe@debian.org>  Sat, 13 Jan 2018 06:37:59 +0100

clinfo (2.1.16.01.12-1) unstable; urgency=medium

  * New upstream release.
  * Update Vcs-Git URL.
  * Bump Standards-Version to 3.9.7, no changes needed.

 -- Andreas Beckmann <anbe@debian.org>  Wed, 17 Feb 2016 14:06:43 +0100

clinfo (2.0.15.04.28-1) unstable; urgency=medium

  * New upstream release.
  * Upload to unstable.

 -- Andreas Beckmann <anbe@debian.org>  Tue, 28 Apr 2015 14:29:39 +0200

clinfo (0.0.git20141116-g3263181-1) experimental; urgency=medium

  * Switch to the implementation by Giuseppe Bilotta,
    https://github.com/Oblomov/clinfo
  * Redo the packaging for the new upstream.
  * Upload to experimental.

 -- Andreas Beckmann <anbe@debian.org>  Mon, 17 Nov 2014 06:21:53 +0100

clinfo (0.0.20130513-2) unstable; urgency=medium

  [ Andreas Beckmann ]
  * Set Maintainer to "Debian OpenCL Maintainers" with Simon's permission.
  * Add Simon Richter and myself to Uploaders.
  * Import packaging into git. Add Vcs-* URLs.
  * Bump Standards-Version to 3.9.6, no changes needed.

  [ Rebecca N. Palmer ]
  * Don't exit on CL_DEVICE_NOT_FOUND (Closes: #767985)
  * Fix typo in "endianness".

 -- Andreas Beckmann <anbe@debian.org>  Thu, 13 Nov 2014 12:16:40 +0100

clinfo (0.0.20130513-1.1) unstable; urgency=medium

  * Non-maintainer upload.

  [ Andreas Boll ]
  * Drop unused CL_DEVICE_BUILT_IN_KERNELS query, fixes clinfo output on
    OpenCL 1.1 drivers (Closes: #721103).

 -- Julien Cristau <jcristau@debian.org>  Mon, 15 Sep 2014 18:02:53 +0200

clinfo (0.0.20130513-1) unstable; urgency=low

  * New upstream release

 -- Simon Richter <sjr@debian.org>  Mon, 13 May 2013 19:52:36 +0200

clinfo (0.0.20130409-2) unstable; urgency=low

  * Conflict/Replace amd-clinfo (Closes: #706093)

 -- Simon Richter <sjr@debian.org>  Fri, 26 Apr 2013 12:02:59 +0200

clinfo (0.0.20130409-1) unstable; urgency=low

  * Initial release

 -- Simon Richter <sjr@debian.org>  Tue, 09 Apr 2013 22:38:26 +0200
